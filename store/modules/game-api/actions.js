import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_FIRST_DATA]: (store) => {
        return axios.get(apiUrls.DO_FIRST_DATA)
    },
    [Constants.DO_PICK_THE_GRASS]: (store) => {
        return axios.put(apiUrls.DO_PICK_THE_GRASS)
    },
    [Constants.DO_PICK_NORMAL_POT]: (store) => {
        return axios.post(apiUrls.DO_PICK_NORMAL_POT)
    },
    [Constants.DO_PICK_WITCH_POT]: (store) => {
        return axios.post(apiUrls.DO_PICK_WITCH_POT)
    },
    [Constants.DO_CLEAR_ALL]: (store) => {
        return axios.delete(apiUrls.DO_CLEAR_ALL)
    },



}
