export default {
    DO_FIRST_DATA: 'game-api/doFirstData',
    DO_PICK_THE_GRASS : 'game-api/doPickTheGrass',
    DO_PICK_NORMAL_POT : 'game-api/doPickNormalPot',
    DO_PICK_WITCH_POT : 'game-api/doPickWitchPot',
    DO_CLEAR_ALL : 'game-api/doClearAll',
}
