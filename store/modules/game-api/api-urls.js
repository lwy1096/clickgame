const BASE_URL = '/v1'

export default {
    DO_FIRST_DATA: `${BASE_URL}/first-connect/init`, //get
    DO_PICK_THE_GRASS: `${BASE_URL}/grass/plus`, //put
    DO_PICK_NORMAL_POT: `${BASE_URL}/pickup-potion/normal`, //put
    DO_PICK_WITCH_POT: `${BASE_URL}/pickup-potion/witch`, //put
    DO_CLEAR_ALL: `${BASE_URL}/first-connect/reset`, //del

}
